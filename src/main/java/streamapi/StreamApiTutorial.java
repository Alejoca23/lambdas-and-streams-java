package streamapi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamApiTutorial {
    static List<Employee> employeeList = new ArrayList<>();

    static {
        employeeList.add(
                new Employee("Bill", "Gates", 80_000.0, List.of("Microsoft1", "Microsoft2"))
        );
        employeeList.add(
                new Employee("Larry", "Page", 30_000.0, List.of("Google1", "Google2"))
        );
        employeeList.add(
                new Employee("Serguéi", "Brin", 25_000.0, List.of("Google1", "Google3"))
        );
    }

    public static void main(String[] args) {
        //INTERMEDIATE OPERATION
        //Stream Method
//        Stream.of(employeeList);
        //forEach
        employeeList.stream()
                .forEach(employee -> System.out.println(employee));
        System.out.println("---------------");
        //map
        //collect
        Set<Employee> increasedSal = employeeList.stream()
                .map(employee -> new Employee(
                        employee.getFirstName(),
                        employee.getLastName(),
                        employee.getSalary() * 1.16,
                        employee.getProjects()
                )).collect(Collectors.toSet());
        System.out.println(increasedSal);
        System.out.println("---------------");

        //COLLECTING DATA
        //filter
        List<Employee> filterEmployee = employeeList.stream()
                .filter(employee -> employee.getSalary() > 28_000)
                .map(employee -> new Employee(
                        employee.getFirstName(),
                        employee.getLastName(),
                        employee.getSalary() * 1.16,
                        employee.getProjects()
                ))
                .collect(Collectors.toList());
        System.out.println(filterEmployee);
        System.out.println("---------------");

        //MORE INTERMEDIATE & TERMINAL OPERATIONS
        //findFirst
        Employee firstEmployee = employeeList.stream()
                .filter(employee -> employee.getSalary() > 100_000)
                .map(employee -> new Employee(
                        employee.getFirstName(),
                        employee.getLastName(),
                        employee.getSalary() * 1.16,
                        employee.getProjects()
                ))
                .findFirst()
                .orElse(null);
        System.out.println(firstEmployee);
        System.out.println("---------------");

        //flatMap
        String projects = employeeList.stream()
                .map(employee -> employee.getProjects())
                .flatMap(project -> project.stream())
                .collect(Collectors.joining(", "));
        System.out.println(projects);
        System.out.println("---------------");

        //SHORT CIRCUIT OPERATION
    }
}
