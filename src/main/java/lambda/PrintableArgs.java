package lambda;
@FunctionalInterface
public interface PrintableArgs {
    void print(String prefix, String suffix);
}
