package lambda;

public class Lambdas {
    public static void main(String[] args) {
        //With this implementation the function need to receive myCat object
        Cat myCat = new Cat();
        //myCat.print();
        printThing(myCat);

        //Lambda: Instead of passing the object that has the implementation, you just pass it in the specific implementation directly
        Printable lambdaPrintable = () -> System.out.println("Meow Lambda");
        printThing(lambdaPrintable);
//        printThing(() -> System.out.println("Meow Lambda"));


        //Lambda with params
        PrintableArgs lambdaPrintableArgs = (p, s) -> System.out.printf("%sMeow%s\n", p, s);
        printArgsThing(lambdaPrintableArgs);

        //Return lambda
        PrintableReturn lambdaPrintableReturn = s -> "Meow " + s;
        printReturnThing(lambdaPrintableReturn);
    }

    static void printThing(Printable thing) {
        thing.print();
    }

    static void printArgsThing(PrintableArgs thing) {
        thing.print(":", "!");
    }

    static void printReturnThing(PrintableReturn thing) {
        System.out.println(thing.print("!!!"));
    }
}
