package lambda;

@FunctionalInterface
public interface PrintableReturn {
    String print( String suffix);
}
